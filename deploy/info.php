<?php

/////////////////////////////////////////////////////////////////////////////
// General information
/////////////////////////////////////////////////////////////////////////////

$app['basename'] = 'network_map';
$app['version'] = '2.5.9';
$app['vendor'] = 'ClearCenter';
$app['packager'] = 'ClearCenter';
$app['license'] = 'Proprietary';
$app['license_core'] = 'Proprietary';
$app['description'] = lang('network_map_app_description');

/////////////////////////////////////////////////////////////////////////////
// App name and categories
/////////////////////////////////////////////////////////////////////////////

$app['name'] = lang('network_map_app_name');
$app['category'] = lang('base_category_network');
$app['subcategory'] = lang('base_subcategory_device_management');

/////////////////////////////////////////////////////////////////////////////
// Controllers
/////////////////////////////////////////////////////////////////////////////

$app['controllers']['network_map']['title'] = lang('network_map_app_name');
$app['controllers']['mapped']['title'] = lang('network_map_mapped_devices');
$app['controllers']['unknown_summary']['title'] = lang('network_map_unknown_devices');
$app['controllers']['subscription']['title'] = lang('base_subscription');

/////////////////////////////////////////////////////////////////////////////
// Packaging
/////////////////////////////////////////////////////////////////////////////

$app['requires'] = array(
    'app-network',
    'app-tasks-core',
);

// app-openldap-core required for schema additions
$app['core_requires'] = array(
    'app-base-core >= 1:2.0.13',
    'app-clearcenter-core >= 1:1.4.70',
    'app-events-core',
    'app-ldap-core >= 1:1.4.70',
    'app-network-core >= 1:1.6.2',
    'app-openldap-core >= 1:1.4.70',
    'app-mode-core >= 1:1.4.70',
    'arpwatch'
);

$app['core_directory_manifest'] = array(
    '/var/clearos/network_map' => array(),
);

$app['core_file_manifest'] = array(
    'arpwatch.php'=> array('target' => '/var/clearos/base/daemon/arpwatch.php'),
    'app-network-map.cron' => array('target' => '/etc/cron.d/app-network-map'),
    'network_map_subscription' =>  array('target' => '/var/clearos/clearcenter/subscriptions/network_map'),
    'rsyslogd-network-map.conf' => array('target' => '/etc/rsyslog.d/network-map.conf'),
    'network-configuration-event'=> array(
        'target' => '/var/clearos/events/network_configuration/network_map',
        'mode' => '0755'
    ),
    'export-network-map' => array(
        'target' => '/usr/sbin/export-network-map',
        'mode' => '0755',
    ),
);

/////////////////////////////////////////////////////////////////////////////
// App Removeal Dependencies
/////////////////////////////////////////////////////////////////////////////

$app['delete_dependency'] = array();
