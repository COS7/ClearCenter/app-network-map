To update the MAC address database, update it in app-dhcp first then copy it over to deploy/mac_database.php.

To update ethercodes for arpwatch, from the deploy folder, run `wget -Nq https://linuxnet.ca/ieee/oui/ethercodes.dat`
