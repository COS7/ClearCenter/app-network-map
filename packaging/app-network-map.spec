
Name: app-network-map
Epoch: 1
Version: 2.5.9
Release: 1%{dist}
Summary: Network Map
License: Proprietary
Group: Applications/Apps
Packager: ClearCenter
Vendor: ClearCenter
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base
Requires: app-network
Requires: app-tasks-core

%description
The Network Map scans then network for devices and provides a way to associate devices with specific end users.

%package core
Summary: Network Map - API
License: Proprietary
Group: Applications/API
Requires: app-base-core
Requires: app-base-core >= 1:2.0.13
Requires: app-clearcenter-core >= 1:1.4.70
Requires: app-events-core
Requires: app-ldap-core >= 1:1.4.70
Requires: app-network-core >= 1:1.6.2
Requires: app-openldap-core >= 1:1.4.70
Requires: app-mode-core >= 1:1.4.70
Requires: arpwatch

%description core
The Network Map scans then network for devices and provides a way to associate devices with specific end users.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/network_map
cp -r * %{buildroot}/usr/clearos/apps/network_map/
rm -f %{buildroot}/usr/clearos/apps/network_map/README.md

install -d -m 0755 %{buildroot}/var/clearos/network_map
install -D -m 0644 packaging/app-network-map.cron %{buildroot}/etc/cron.d/app-network-map
install -D -m 0644 packaging/arpwatch.php %{buildroot}/var/clearos/base/daemon/arpwatch.php
install -D -m 0755 packaging/export-network-map %{buildroot}/usr/sbin/export-network-map
install -D -m 0755 packaging/network-configuration-event %{buildroot}/var/clearos/events/network_configuration/network_map
install -D -m 0644 packaging/network_map_subscription %{buildroot}/var/clearos/clearcenter/subscriptions/network_map
install -D -m 0644 packaging/rsyslogd-network-map.conf %{buildroot}/etc/rsyslog.d/network-map.conf

%post
logger -p local6.notice -t installer 'app-network-map - installing'

%post core
logger -p local6.notice -t installer 'app-network-map-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/network_map/deploy/install ] && /usr/clearos/apps/network_map/deploy/install
fi

[ -x /usr/clearos/apps/network_map/deploy/upgrade ] && /usr/clearos/apps/network_map/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-network-map - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-network-map-api - uninstalling'
    [ -x /usr/clearos/apps/network_map/deploy/uninstall ] && /usr/clearos/apps/network_map/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/network_map/controllers
/usr/clearos/apps/network_map/htdocs
/usr/clearos/apps/network_map/views

%files core
%defattr(-,root,root)
%doc README.md
%exclude /usr/clearos/apps/network_map/packaging
%exclude /usr/clearos/apps/network_map/unify.json
%dir /usr/clearos/apps/network_map
%dir /var/clearos/network_map
/usr/clearos/apps/network_map/deploy
/usr/clearos/apps/network_map/language
/usr/clearos/apps/network_map/libraries
/etc/cron.d/app-network-map
/var/clearos/base/daemon/arpwatch.php
/usr/sbin/export-network-map
/var/clearos/events/network_configuration/network_map
/var/clearos/clearcenter/subscriptions/network_map
/etc/rsyslog.d/network-map.conf
